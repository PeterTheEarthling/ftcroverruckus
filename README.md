### Overview
This is team #11115's source code for the Rover Ruckus season of the First Tech Challenge Robotics competition.

Robot logic can be found under ftc_app-master/TeamCode/src/main/java/

Computer vision logic can be found under ftc_app-master/TeamCode/src/main/cpp/

### Orbiting Around Lander

The core of our autonomous feeding algorithm takes place through what we call orbit mode. This algorithm’s purpose is simple: move to optimal feeding alignment as quickly as possible while maintaining stability. This only involves movement in the vertical direction and turning, leaving one dimension free: strafing. Since the algorithm doesn’t apply any strafing power to the drivetrain, setting the straif power moves the robot to the right or left while orbit mode maintains optimal alignment.
The biggest problem we fear with any crater-based robot is defense. Simply by getting in the way at strategic times, another robot could in theory completely take us down as it is mandatory that we move out of the way if they intend to pass by. Orbit mode is used to strafe out of the way while not needing to stop the feeding process since the dumper stays in the same location.


![picture](images/OrbitMode.png)

### Custom PID Control (135 Updates per Second)

To drive our lift at absolutely maximum speed, we needed to do our own PID control through software. This process is especially risky for several reasons: the phone is not a real-time OS, meaning the number of times the program executes per second is very fluid (due to background tasks, interruptions, and other issues), second hardware writes are incredibly slow through the control system and the more of them are done, the slower the loop time. We have optimized this through large priority lists that minimize the amount of accessing the hardware and maximizing the loop time as well as reading all sensors and motor encoders in one bulk read (capable through a library called REV-extensions 2). This results in much faster update rates and the ability to do our own PID control of the lift extension.

The control algorithm itself is similar to a standard PID loop with some nonlinear control. The nonlinear portion can pause the integral (error-summing) when the loop is railed (attempting to go to a target position too far out of its stability range). This allows us to floor the power for most of the range whereas the built in PIDs to the REV control system would normally prevent speeds greater than 80% of the motor to avoid railing.

### Active Braking - Issues with the REV Control System
One of the biggest problems in our control is stopping the robot or a component of the robot very quickly. With the REV control system, unlike the Modern Robotics, the only way to get active braking is to set the power of a motor to exactly 0. Anything slightly more or less than this will not apply any breaking. Our control algorithms almost never set the power to exactly 0 for there is always error in the system, meaning we can’t utilize any inherent braking like we could with the MR control system earlier in the season (without doing a jerky PWM control loop which would run much too slowly due to the blocking hardware reads and writes and low update times). The solution around this was to do completely manual breaking control. To do this we incorporate an element of prediction when needing to stop a load quickly. The process involves measuring the speed and predicting the position of the system a small amount of time in the future. We can then apply power in the direction of the target point. The result is that we can stop extremely quickly and near full power, a process that would otherwise need to be much slower without the prediction element.

![picture](images/ActiveBraking.png)

### Autonomous Feeding

A large portion of our teleop control this year is completely automatic. Mainly, the feeding process. Our dual lift system has encoders on both the feeder extension and the collector extension. These are controlled by software instead of the user. It is faster and safer than manual control. 

When started, the auto-feed state machine takes control away from the user and moves the robot itself. This is done through a class named CollectorLiftManger, which contains references to the collector extension controller, lift feeder controller, and can even override the user’s movement. Of course if anything goes wrong this state machine can be canceled if the user pushes a button. 

![picture](images/AutonomousFeeding.png)

### Absolute Collector Position Control
One of the issues we have had with controlling the robot between two drivers this year is managing where the collector extension is located. When driver 1 would apply forward movement and driver 2 also extended the collector, the combined acceleration was unmanageable. Driver communication was not very practical because it would have to happen too often, so we made a new algorithm:

This algorithm is designed to cancel the movements of driver 1. Driver 2 has complete independent control of the collector extension. All the driver has to do is hold down a button and using odometry, all driver 1’s movements are completely canceled.

![picture](images/AbsoluteCollectorPositionControl.png)


### Continuous Movement Through Paths
To create fluid movement, there can’t be stops along the way. Almost everything should be connected. This first requirement is actually extremely difficult. Our autonomous path is made up of many pass-through points on the field: defining coordinates of the robot’s path. Last year, the only option we had was to stop at all these points and or patch up the junction with inaccurate hacks. 
 
This year our movement is completely different. Every 2 key points define a straight line. This line is actually just a guideline and the robot’s path can often be quite different.

![picture](images/ContinuousMovementThroughPaths.png)


